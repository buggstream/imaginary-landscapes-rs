use argh::{FromArgValue, FromArgs};
use futures::{stream, StreamExt};
use image::{DynamicImage, GenericImageView};
use roux::response::BasicThing;
use roux::submission::SubmissionData;
use roux::util::{FeedOption, TimePeriod};
use roux::Subreddit;
use tokio::runtime::Runtime;
use url::Url;

const SUBREDDIT: &str = "ImaginaryLandscapes";
const PRINT_ERRORS: bool = false;

fn main() -> anyhow::Result<()> {
    let args: ScraperArgs = argh::from_env();

    let rt = Runtime::new()?;
    rt.block_on(scrape_reddit(args))
}

async fn scrape_reddit(args: ScraperArgs) -> anyhow::Result<()> {
    let posts = top_posts(args.period(), args.limit()).await?;

    let images_to_process = posts
        .iter()
        .filter_map(|post| post.data.url.as_ref())
        .map(|url| process_image(url));

    let mut buffered = stream::iter(images_to_process).buffer_unordered(args.parallel_jobs());

    while let Some(result) = buffered.next().await {
        match result {
            Ok(filename) => println!("Finished processing ({})", &filename),
            Err(err) => {
                if PRINT_ERRORS {
                    println!("{}", err);
                }
            }
        }
    }

    Ok(())
}

async fn top_posts(
    period: TimePeriod,
    limit: u32,
) -> anyhow::Result<Vec<BasicThing<SubmissionData>>> {
    let subreddit = Subreddit::new(SUBREDDIT);
    let feed = FeedOption::new().period(period);
    let top_results = subreddit.top(limit, Some(feed)).await?;

    Ok(top_results.data.children)
}

async fn process_image(url: &str) -> anyhow::Result<String> {
    let (image_name, image) = extract_image(url).await?;
    let (width, height) = image.dimensions();

    if width < 1920 {
        return Err(anyhow::Error::msg(format!(
            "Image width too small ({})",
            url
        )));
    }

    let ratio: f64 = width as f64 / height as f64;

    if ratio < 1.67 || ratio > 1.9 {
        return Err(anyhow::Error::msg(format!(
            "Image ratio is not close enough to 16:9 ratio ({})",
            url
        )));
    }

    let filename = format!("images/{}", image_name);
    image.save(&filename)?;

    Ok(filename)
}

async fn extract_image(url: &str) -> anyhow::Result<(String, DynamicImage)> {
    let image_name = extract_image_name(url)?;
    let response = reqwest::get(url).await?;
    let bytes = response.bytes().await?;
    Ok((image_name, image::load_from_memory(&bytes)?))
}

fn extract_image_name(url: &str) -> anyhow::Result<String> {
    let parsed_url = Url::parse(url)?;
    parsed_url
        .path_segments()
        .and_then(|segments| segments.last())
        .map(|segment| segment.to_string())
        .ok_or(anyhow::Error::msg(format!(
            "No last path segment found ({})",
            url
        )))
}

/// Scrape images from the ImaginaryLandscapes subreddit
#[derive(FromArgs)]
struct ScraperArgs {
    /// the time period for which the top results should be shown.
    /// valid values are: now, day/today, week, month, year, all/alltime
    #[argh(option, default = "TimePeriodArg(TimePeriod::ThisMonth)")]
    period: TimePeriodArg,
    /// the maximum amount of posts that can be scraped
    #[argh(option, default = "100")]
    limit: u32,
    /// the maximum amount of images that are processed in parallel
    #[argh(option, default = "5")]
    parallel_jobs: usize,
}

impl ScraperArgs {
    pub fn period(&self) -> TimePeriod {
        self.period.0
    }

    pub fn limit(&self) -> u32 {
        self.limit
    }

    pub fn parallel_jobs(&self) -> usize {
        self.parallel_jobs
    }
}

struct TimePeriodArg(TimePeriod);

impl FromArgValue for TimePeriodArg {
    fn from_arg_value(value: &str) -> Result<Self, String> {
        match value.to_lowercase().as_str() {
            "now" => Ok(TimePeriodArg(TimePeriod::Now)),
            "day" | "today" => Ok(TimePeriodArg(TimePeriod::Today)),
            "week" => Ok(TimePeriodArg(TimePeriod::ThisWeek)),
            "month" => Ok(TimePeriodArg(TimePeriod::ThisMonth)),
            "year" => Ok(TimePeriodArg(TimePeriod::ThisYear)),
            "all" | "alltime" => Ok(TimePeriodArg(TimePeriod::AllTime)),
            _ => Err(String::from("Invalid period name!")),
        }
    }
}
